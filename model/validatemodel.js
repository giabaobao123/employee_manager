var validator = {
  kiemTraDoDai: function (string, idErr, min, max) {
    if (string.length < min || string.length > max) {
      domID(idErr).innerText = ` * Độ dài từ ${min} - ${max} ký tự`;
      return false;
    } else {
      domID(idErr).innerHTML = "";
      return true;
    }
  },
  kiemTraRong: function (string, idErr) {
    if (string == "") {
      domID(idErr).innerText = " * Trường này không được để rỗng";
      return false;
    } else {
      domID(idErr).innerText = "";
      return true;
    }
  },
  kiemTraChuoi: function (string, idErr) {
    var regex =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (regex.test(string)) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = " * Tên nhân viên phải là chữ";
      return false;
    }
  },
  kiemTraEmail: function (string, idErr) {
    var regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(string)) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = " * Email chưa đúng định dạng";
      return false;
    }
  },
  kiemTraTrungTaiKhoan: function (string, dsnv, idErr) {
    var index = dsnv.findIndex((nv) => {
      return nv.taiKhoan == string;
    });
    if (index == -1 || index == undefined) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = ` * ${string} đã tồn tại`;
      return false;
    }
  },
  kiemTraMatKhau: function (string, idErr) {
    var regex =
      /^(?=.*[A-Z])(?=.*[0-9])(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])/;
    if (regex.test(string)) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText =
        " * Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
      return false;
    }
  },
  kiemTraNgay: function (string, idErr) {
    var regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (regex.test(string)) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = " * Phải đúng định dạng mm/dd/yyyy";
      return false;
    }
  },
  kiemTraLuong: function (string, idErr) {
    if (string < 1e6 || string > 20e6) {
      domID(idErr).innerText = " * Lương cơ bản 1 000 000 - 20 000 000 ";
      return false;
    }
    return true;
  },
  kiemTraChucVu: function (string, idErr) {
    if (string == 1 || string == 2 || string == 3) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = " * Chưa chọn chức vụ";
      return false;
    }
  },
  kiemTraGioLam: function (string, idErr) {
    if (string < 80 || string > 200) {
      domID(idErr).innerText = " * Số giờ làm trong tháng 80 - 200 giờ";
      return false;
    }
    return true;
  },
  kiemTraKhoangTrang: function (string, idErr) {
    var regex = /^[a-zA-Z0-9_]*$/;
    if (regex.test(string)) {
      domID(idErr).innerText = "";
      return true;
    } else {
      domID(idErr).innerText = " * Giữa các ký tự không được có khoảng trắng";
      return false;
    }
  },
};

// validate
function validation() {
  if (
    validateTk() &
    validateTen() &
    validateEmail() &
    validateMatKhau() &
    validateNgayLam() &
    validateLuong() &
    validateChucVu() &
    validateGioLam()
  ) {
    return true;
  } else {
    return false;
  }
}
function validateTk() {
  var taiKhoanNv = domID("tknv").value.trim();
  var isValid =
    validator.kiemTraRong(taiKhoanNv, "tbTKNV") &&
    validator.kiemTraKhoangTrang(taiKhoanNv, "tbTKNV") &&
    validator.kiemTraDoDai(taiKhoanNv, "tbTKNV", 4, 6) &&
    validator.kiemTraTrungTaiKhoan(taiKhoanNv, dsnv, "tbTKNV");
  return isValid;
}
function validateTen() {
  var tenNv = domID("name").value.trim();
  var isValid =
    validator.kiemTraRong(tenNv, "tbTen") &&
    validator.kiemTraChuoi(tenNv, "tbTen");
  return isValid;
}
function validateEmail() {
  var emailNv = domID("email").value.trim();
  var isValid =
    validator.kiemTraRong(emailNv, "tbEmail") &&
    validator.kiemTraEmail(emailNv, "tbEmail");
  return isValid;
}
function validateMatKhau() {
  var matKhauNv = domID("password").value.trim();
  var isValid =
    validator.kiemTraRong(matKhauNv, "tbMatKhau") &&
    validator.kiemTraDoDai(matKhauNv, "tbMatKhau", 6, 10) &&
    validator.kiemTraMatKhau(matKhauNv, "tbMatKhau");
  return isValid;
}
function validateNgayLam() {
  var ngayLamNv = domID("datepicker").value.trim();
  var isValid =
    validator.kiemTraRong(ngayLamNv, "tbNgay") &&
    validator.kiemTraNgay(ngayLamNv, "tbNgay");
  return isValid;
}
function validateLuong() {
  var luongCbNv = domID("luongCB").value.trim() * 1;
  var isValid =
    validator.kiemTraRong(luongCbNv, "tbLuongCB") &&
    validator.kiemTraLuong(luongCbNv, "tbLuongCB");
  return isValid;
}
function validateChucVu() {
  var chucVuNv = domID("chucvu").value.trim();
  var isValid = validator.kiemTraChucVu(chucVuNv, "tbChucVu");
  return isValid;
}
function validateGioLam() {
  var gioLamNv = domID("gioLam").value.trim() * 1;
  var isValid =
    validator.kiemTraRong(gioLamNv, "tbGiolam") &&
    validator.kiemTraGioLam(gioLamNv, "tbGiolam");
  return isValid;
}
// end validate
function removeVietnameseTones(str) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  str = str.replace(/ + /g, " ");
  str = str.trim();
  return str;
}
