function NhanVien(
  taiKhoanNv,
  tenNv,
  emailNv,
  matKhauNv,
  ngayLamNv,
  luongCbNv,
  chucVuNv,
  gioLamNv
  ) {
  this.taiKhoan = taiKhoanNv;
  this.ten = tenNv;
  this.email = emailNv;
  this.matKhau = matKhauNv;
  this.ngayLam = ngayLamNv;
  this.luongCb = luongCbNv;
  this.ValueChucVu = chucVuNv;
  this.gioLam = gioLamNv;
  this.chucVu = function () {
    var tenChucVu = "";
    if (this.ValueChucVu == 3) {
      tenChucVu = "Sếp";
    } else if (this.ValueChucVu == 2) {
      tenChucVu = "Trưởng Phòng";
    } else if (this.ValueChucVu == 1) {
      tenChucVu = "Nhân Viên";
    }
    return tenChucVu;
  };
  this.xepLoai = function () {
    var xepLoai = "";
    if (this.gioLam >= 192) {
      xepLoai = "Xuất Sắc";
    } else if (this.gioLam >= 176) {
      xepLoai = "Gioi";
    } else if (this.gioLam >= 160) {
      xepLoai = "Khá";
    } else {
      xepLoai = "Trung Bình";
    }
    return xepLoai;
  };
  this.tongLuong = function () {
    return this.luongCb * this.ValueChucVu;
  };
}
function domID(id) {
  return document.getElementById(id);
}
