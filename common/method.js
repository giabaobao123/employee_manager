function layThongTinTuForm() {
  var taiKhoanNv = domID("tknv").value.trim();
  var tenNv = domID("name").value.trim();
  var emailNv = domID("email").value.trim();
  var matKhauNv = domID("password").value.trim();
  var ngayLamNv = domID("datepicker").value.trim();
  var luongCbNv = domID("luongCB").value.trim() * 1;
  var chucVuNv = domID("chucvu").value.trim();
  var gioLamNv = domID("gioLam").value.trim() * 1;

  return new NhanVien(
    taiKhoanNv,
    tenNv,
    emailNv,
    matKhauNv,
    ngayLamNv,
    luongCbNv,
    chucVuNv,
    gioLamNv
  );
}
function inDsnv(arr) {
  var inDsnv = ``;
  for (var i = 0; i < arr.length; i++) {
    var dsnv1hang = `
    <tr>
    <td>${arr[i].taiKhoan}</td>
    <td>${arr[i].ten}</td>
    <td>${arr[i].email}</td>
    <td>${arr[i].ngayLam}</td>
    <td>${arr[i].chucVu()}</td>
    <td>${arr[i].tongLuong().toLocaleString()} VND</td>
    <td>${arr[i].xepLoai()}</td>
    <td>
    <button class="btn btn-success w-50 mb-3" onclick="xoaNv('${
      arr[i].taiKhoan
    }')">Xóa</button>
    <button class="btn btn-success w-50" data-toggle="modal" data-target="#myModal" onclick="suaNv('${
      arr[i].taiKhoan
    }')">Sửa</button>
    </td>
    </tr>
    `;
    inDsnv += dsnv1hang;
  }
  document.querySelector("#tableDanhSach").innerHTML = inDsnv;
}
function viTriNv(id, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].taiKhoan == id) {
      return i;
    }
  }
  return -1;
}

function showThongTinNvLenForm(nv) {
  domID("tknv").value = nv.taiKhoan;
  domID("name").value = nv.ten;
  domID("email").value = nv.email;
  domID("password").value = nv.matKhau;
  domID("datepicker").value = nv.ngayLam;
  domID("luongCB").value = nv.luongCb;
  domID("chucvu").value = nv.ValueChucVu;
  domID("gioLam").value = nv.gioLam;
}

function btnthem() {
  domID("btnThemNV").style.display = "inline";
  domID("btnCapNhat").style.display = "none";
  domID("tknv").disabled = false;
}
