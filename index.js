var dsnv = [];
//Lấy localstorage
var dsnvlocalStorage = localStorage.getItem("DSNV");
if (JSON.parse(dsnvlocalStorage)) {
  var dataNv = JSON.parse(dsnvlocalStorage);
  dataNv.forEach(function (dsnvItem) {
    var nv = new NhanVien(
      dsnvItem.taiKhoan,
      dsnvItem.ten,
      dsnvItem.email,
      dsnvItem.matKhau,
      dsnvItem.ngayLam,
      dsnvItem.luongCb,
      dsnvItem.ValueChucVu,
      dsnvItem.gioLam
    );
    dsnv.push(nv);
  });
  inDsnv(dsnv);
}
// Save localstorage
function saveLocalStorage() {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", dsnvJson);
}
// Thêm NV----------------------------------------------
function themNv() {
  var newNv = layThongTinTuForm();
  var isValid = validation();
  console.log("isValid: ", isValid);
  if (isValid == false) {
    return;
  }
  dsnv.push(newNv);
  saveLocalStorage();
  inDsnv(dsnv);
  alert("Thêm nhân viên thành công");
  domID("myform").reset();
}
console.log(dsnv)
//Xóa Nhân viên--------------------------------------------
function xoaNv(id) {
  var index = viTriNv(id, dsnv);
  if (index !== -1) {
    dsnv.splice(index, 1);
    saveLocalStorage();
    inDsnv(dsnv);
  }
}
//Sửa Nhân Viên------------------------------------------
function suaNv(id) {
  domID("btnThemNV").style.display = "none";
  domID("btnCapNhat").style.display = "inline";
  domID("tknv").disabled = true;
  var index = viTriNv(id, dsnv);
  if (index !== -1) {
    showThongTinNvLenForm(dsnv[index]);
  }
}
// Cập Nhật Nhân Viên--------------------------------------
function capNhatNv() {
  var nvCapNhat = layThongTinTuForm();
  var index = viTriNv(nvCapNhat.taiKhoan, dsnv);
  if (index !== -1) {
    dsnv[index] = nvCapNhat;
    saveLocalStorage();
    inDsnv(dsnv);
    alert("Cập nhật nhân viên thành công");
    domID("myform").reset();
  }
}
// Search Nhân Viên-------------------------------------------
function searchNv() {
  var searchNv = domID("searchName").value;
  searchNv = removeVietnameseTones(searchNv).toUpperCase();
  var dsnvSearch = [];
  if (searchNv) {
    dsnv.forEach(function (item) {
      var soSanhTimKiemNv = item.xepLoai();
      soSanhTimKiemNv = removeVietnameseTones(soSanhTimKiemNv).toUpperCase();
      if (soSanhTimKiemNv.includes(searchNv)) {
        dsnvSearch.push(item);
        inDsnv(dsnvSearch);
      }
    });
    inDsnv(dsnvSearch);
  }
}
function removeSearch() {
  inDsnv(dsnv);
}
